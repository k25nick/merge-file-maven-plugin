/*
 * Copyright 2016 Nick Wilson
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.k25nick.mergefile;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.codehaus.plexus.component.annotations.Component;

/**
 * Class for merging multiple source files into a single target file.
 */
@Component(role = FileMerger.class)
public class FileMerger
{

	/**
	 * Merge all of the source files in order into the target file.
	 * 
	 * @param sources Sources to merge.
	 * @param target target to write output to.
	 * @throws IOException if there is an error reading from a source file or writing to the target file.
	 */
	public void mergeFiles(List<Source> sources, Target target) throws IOException
	{
		byte[] buffer = new byte[2048];
		try (OutputStream out = target.getOutputStream())
		{
			for (Source source : sources)
			{
				try (InputStream is = source.getInputStream())
				{
					int c = 0;
					while ((c = is.read(buffer)) != -1)
					{
						out.write(buffer, 0, c);
					}
				}
			}
		}
	}
}
