/*
 * Copyright 2016 Nick Wilson
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.k25nick.mergefile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.shared.model.fileset.FileSet;
import org.apache.maven.shared.model.fileset.util.FileSetManager;
import org.codehaus.plexus.util.FileUtils;

/**
 * Mojo for merging files.
 */
@Mojo(name = "merge", defaultPhase = LifecyclePhase.PACKAGE)
public class FileMergeMojo extends AbstractMojo
{

	/**
	 * The source folder.
	 */
	@Parameter(property = "sourceFiles", required = true)
	private List<FileSet>	sourceFiles;

	/**
	 * Project relative output folder for generated code.
	 */
	@Parameter(property = "targetFile", required = true)
	private String			targetFile;

	/**
	 * Base directory to work from.
	 */
	@Parameter(property = "basedir", defaultValue = "${basedir}")
	private String			basedir;

	@Component
	private FileMerger		merger;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException
	{
		List<Source> files = new ArrayList<>();
		FileSetManager fileSetManager = new FileSetManager();
		if (targetFile != null)
		{
			if (sourceFiles != null)
			{
				File base = new File(basedir);
				for (FileSet fileSet : sourceFiles)
				{
					String fileSetDir = fileSet.getDirectory();
					if (fileSetDir == null)
					{
						fileSetDir = basedir + "/src/main/resources";
						fileSet.setDirectory(fileSetDir);
					}
					File fileSetBase = new File(fileSetDir);
					getLog().info("Looking in " + fileSetDir);
					String[] includedFiles = fileSetManager.getIncludedFiles(fileSet);
					if (includedFiles != null && includedFiles.length > 0)
					{
						for (String includedFile : includedFiles)
						{
							getLog().info(" Adding " + includedFile);
							files.add(new FileSource(FileUtils.resolveFile(fileSetBase, includedFile)));
						}
					}
					else
					{
						getLog().info(" No files found.");
					}
				}
				File target = new File(targetFile);
				if (!target.isAbsolute())
				{
					target = new File(base, targetFile);
				}
				getLog().info("Writing to " + target.getAbsolutePath());
            try {
				   merger.mergeFiles(files, new FileTarget(target));
            } catch(IOException e) {
               throw new MojoFailureException(e.getMessage());
            }
			}
			else
			{
				getLog().error("No source files defined.");
			}
		}
		else
		{
			getLog().error("No target file defined.");
		}
	}
}
