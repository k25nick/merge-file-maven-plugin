/*
 * Copyright 2016 Nick Wilson
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.k25nick.mergefile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * File based target to provide access to an output stream.
 */
public class FileTarget implements Target
{

	private File file;

	public FileTarget(File file)
	{
		this.file = file;
	}

	@Override
	public OutputStream getOutputStream() throws FileNotFoundException
	{
		if (file == null || file.getParentFile() == null)
		{
			throw new FileNotFoundException();
		}
		file.getParentFile().mkdirs();
		return new FileOutputStream(file);
	}
}
