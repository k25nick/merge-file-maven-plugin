/*
 * Copyright 2016 Nick Wilson
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.k25nick.mergefile.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.shared.model.fileset.FileSet;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.k25nick.mergefile.FileMergeMojo;
import com.k25nick.mergefile.FileMerger;
import com.k25nick.mergefile.Source;
import com.k25nick.mergefile.Target;

public class FileMergeMojoTest
{

	@Spy
	private List<FileSet>					sourceFiles	= new ArrayList<>();

	@Mock
	private FileSet							a;

	@Mock
	private FileMerger						merger;

	@InjectMocks
	private FileMergeMojo					fmm;

	@Captor
	private ArgumentCaptor<List<Source>>	filesCaptor;

	@Before
	public void init()
	{
		MockitoAnnotations.initMocks(this);
		setField(fmm, "targetFile", "");
		setField(fmm, "basedir", "");
	}

	/**
	 * @param fieldName
	 * @param value
	 */
	private void setField(Object target, String fieldName, String value)
	{
		try
		{
			Field field = fmm.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			field.set(fmm, value);
		}
		catch (Exception e)
		{
			fail(e.getMessage());
		}
	}

	@Test
	public void testNoFiles()
	{
		execute();
	}

	@Test
	public void testMergeOneFile()
	{
		sourceFiles.add(a);
		String[] aIncludes = new String[]{"a.txt"};
		String[] aExcludes = new String[]{};
		when(a.getDirectory()).thenReturn("src/test/resources/set1");
		when(a.getIncludesArray()).thenReturn(aIncludes);
		when(a.getExcludesArray()).thenReturn(aExcludes);
		execute();
		try {
         verify(merger).mergeFiles(filesCaptor.capture(), Matchers.any(Target.class));
      } catch (IOException e) {
         fail(e.getMessage());
      }
		List<Source> files = filesCaptor.getValue();
		assertEquals(1, files.size());
	}

	@Test
	public void testExludeOneFile()
	{
		sourceFiles.add(a);
		String[] aIncludes = new String[]{"*.txt"};
		String[] aExcludes = new String[]{"b.txt"};
		when(a.getDirectory()).thenReturn("src/test/resources/set1");
		when(a.getIncludesArray()).thenReturn(aIncludes);
		when(a.getExcludesArray()).thenReturn(aExcludes);
		execute();
		try {
         verify(merger).mergeFiles(filesCaptor.capture(), Matchers.any(Target.class));
      } catch (IOException e) {
         fail(e.getMessage());
      }
   	List<Source> files = filesCaptor.getValue();
		assertEquals(1, files.size());
	}

	/**
	 * 
	 */
	private void execute()
	{
		try
		{
			fmm.execute();
		}
		catch (MojoExecutionException | MojoFailureException e)
		{
			fail(e.getMessage());
		}
	}

}
