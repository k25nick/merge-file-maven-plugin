/*
 * Copyright 2016 Nick Wilson
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.k25nick.mergefile.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.k25nick.mergefile.FileMerger;
import com.k25nick.mergefile.Source;
import com.k25nick.mergefile.Target;

public class FileMergerTest {

    private List<Source> sources;

    @Mock
    private Target       target;

    @Mock
    private Source       a;

    @Mock
    private Source       b;

    private InputStream  ain;

    private InputStream  bin;

    private ByteArrayOutputStream out;

    @InjectMocks
    private FileMerger   fm;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        sources = new ArrayList<>();
        ain = new ByteArrayInputStream("a".getBytes());
        bin = new ByteArrayInputStream("b".getBytes());
        out = new ByteArrayOutputStream();
        try {
            when(a.getInputStream()).thenReturn(ain);
            when(b.getInputStream()).thenReturn(bin);
            when(target.getOutputStream()).thenReturn(out);
        } catch (FileNotFoundException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testMergeFile() {
        sources.add(a);
        sources.add(b);
        try {
            fm.mergeFiles(sources, target);
        } catch (IOException e) {
            fail(e.getMessage());
        }
        String result = new String(out.toByteArray());
        assertEquals("ab", result);
    }

}
